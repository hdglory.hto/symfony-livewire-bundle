# Symfony Livewire
Adaption of the backend part of [Laravel Livewire](https://github.com/livewire/livewire) for the Symfony Framework.

**⚠ Important:** This bundle is currently in the POC (proof of concept) stage. It is in no way recommended for production use!

## Quickstart
based on the [Livewire quickstart](https://laravel-livewire.com/docs/2.x/quickstart) guide.

### Install Livewire
Require the symfony-livewire-bundle package.
```bash
composer require roflcopter24/symfony-livewire-bundle
```

Register the Bundle in your Symfony app `config/bundles.php`:
```php
<?php

return [
    ...
    Terminal42\ServiceAnnotationBundle\Terminal42ServiceAnnotationBundle::class => ['all' => true],
    RoflCopter24\SymfonyLivewireBundle\SymfonyLivewireBundle::class => ['all' => true],
    ...
];
```

Register the route in `config/routes.yaml`:
```yaml
livewire:
    resource: '@SymfonyLivewireBundle/Resources/config/routes.yml'
```

Include the JavaScript in your Twig-Template (on every page that will be using Livewire).
```twig
...
    {{ livewireStyles() }}
</head>
<body>
    ...

    {{ livewireScripts() }}
</body>
</html>
```

Symfony Livewire makes use of service discovery for your components.\
First create a source directory for your components: `src/Component`.

Then make sure it is included in your `config/services.yaml` and enable the use of the ContainerAwareInterface.\
It should look similar to this:
```yaml
services:
  # default configuration for services in *this* file
  _defaults:
    autowire: true      # Automatically injects dependencies in your services.
    autoconfigure: true # Automatically registers your services as commands, event subscribers, etc.
    
  _instanceof:
    Symfony\Component\DependencyInjection\ContainerAwareInterface:
      calls:
        - [ setContainer, [ '@service_container' ] ]

  # makes classes in src/ available to be used as services
  # this creates a service per class whose id is the fully-qualified class name
  App\:
    resource: '../src/'
    public: true
    exclude:
      - '../src/DependencyInjection/'
      - '../src/Entity/'
      - '../src/Kernel.php'
      - '../src/Tests/'
```

Don't forget to clear your application cache!

### Create a component
In the `src/Component` directory create a new PHP file called `Counter.php`:
```php
<?php
declare(strict_types = 1);

namespace App\Component;

use RoflCopter24\SymfonyLivewireBundle\Annotation\Livewire;
use RoflCopter24\SymfonyLivewireBundle\Component\LivewireComponent;

/**
 * @Livewire("counter")
 */
class CounterComponent extends LivewireComponent
{
    public int $count = 0;
    
    public function increment(): void
    {
        $this->count++;
    }
}
```
Note the `@Livewire` class annotation. This specifies the name of the component.


Now create a Twig template for your component in `templates/livewire` called `counter.html.twig`:
```twig
<div style="text-align: center">
    <button wire:click="increment">+</button>
    <h1>{{ count }}</h1>
</div>
```
If no method called `render` is defined in the component class, it will automatically try to use a twig template with the components name from the `templates/livewire` directory (`templates/livewire/<component>.html.twig`).

The components public properties are passed to the template by default.

### Include the component
There are two ways to include your component:
1. By directive `<wire:component-name />`
2. By Twig function `{{ livewire('component-name') }}`

Try it:
```twig
<head>
    ...
    {{ livewireStyles() }}
</head>
<body>
    <wire:counter />

    ...

    {{ livewireScripts() }}
</body>
</html>
```

### View it in the browser
If everything went well, you should now be able to see your component
in the browser and clicking the `+`-Button should increase the counter.
