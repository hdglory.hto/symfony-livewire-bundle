<?php
declare(strict_types=1);

namespace RoflCopter24\SymfonyLivewireBundle\Service;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

class ComponentServicesService implements ComponentServicesInterface
{
    /**
     * @var EventDispatcherInterface
     */
    private EventDispatcherInterface $eventDispatcher;

    /**
     * @var UrlGeneratorInterface
     */
    private UrlGeneratorInterface $urlGenerator;

    /**
     * @var ParameterBagInterface
     */
    private ParameterBagInterface $parameterBag;

    /**
     * @var ContainerInterface
     */
    private ContainerInterface $container;

    public function __construct(EventDispatcherInterface $eventDispatcher, UrlGeneratorInterface $urlGenerator, ParameterBagInterface $parameterBag, ContainerInterface $container)
    {
        $this->eventDispatcher = $eventDispatcher;
        $this->urlGenerator = $urlGenerator;
        $this->parameterBag = $parameterBag;
        $this->container = $container;
    }

    /**
     * @inheritDoc
     */
    public function eventDispatcher(): EventDispatcherInterface
    {
        return $this->eventDispatcher;
    }

    /**
     * @inheritDoc
     */
    public function urlGenerator(): UrlGeneratorInterface
    {
        return $this->urlGenerator;
    }

    /**
     * @inheritDoc
     */
    public function getParameter(string $paramName)
    {
        return $this->parameterBag->get($paramName);
    }

    /**
     * @inheritDoc
     */
    public function hasParameter(string $paramName): bool
    {
        return $this->parameterBag->has($paramName);
    }

    /**
     * @inheritDoc
     */
    public function get(string $name): ?object
    {
        return $this->container->get($name);
    }

    /**
     * @inheritDoc
     */
    public function has(string $name): bool
    {
        return $this->container->has($name);
    }
}
