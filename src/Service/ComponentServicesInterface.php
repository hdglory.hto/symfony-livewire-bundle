<?php
declare(strict_types=1);

namespace RoflCopter24\SymfonyLivewireBundle\Service;

use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

interface ComponentServicesInterface
{
    /**
     * Returns the applications event dispatcher.
     *
     * @return EventDispatcherInterface the default EventDispatcher
     */
    public function eventDispatcher(): EventDispatcherInterface;

    /**
     * Returns the UrlGenerator of the application
     *
     * @return UrlGeneratorInterface the UrlGenerator.
     */
    public function urlGenerator(): UrlGeneratorInterface;

    /**
     * Returns a container parameter.
     *
     * @param string $paramName The name of the parameter.
     * @return mixed the parameter value.
     */
    public function getParameter(string $paramName);

    /**
     * Checks if an app parameter exists.
     * @param string $paramName The name of the parameter.
     * @return bool whether the parameter exists.
     */
    public function hasParameter(string $paramName): bool;

    /**
     * Retrieves a service from the container.
     * @param string $name Name of the service.
     * @return object|null the service or null.
     */
    public function get(string $name): ?object;

    /**
     * Checks if a service exists in the container.
     * @param string $name Name of the service.
     * @return bool whether the service exists.
     */
    public function has(string $name): bool;
}
