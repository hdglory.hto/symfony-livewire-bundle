<?php
declare(strict_types = 1);

namespace RoflCopter24\SymfonyLivewireBundle\DependencyInjection;

use RoflCopter24\SymfonyLivewireBundle\Service\SettingsService;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\HttpKernel\DependencyInjection\ConfigurableExtension;

class SymfonyLivewireExtension extends ConfigurableExtension
{
    /**
     * @inheritDoc
     */
    public function loadInternal(array $mergedConfig, ContainerBuilder $container): void
    {
        $loader = new YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.yaml');

        $definition = $container->getDefinition(SettingsService::class);
        $definition->addArgument($mergedConfig);
    }
}
