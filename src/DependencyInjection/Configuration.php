<?php
declare(strict_types=1);

namespace RoflCopter24\SymfonyLivewireBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{

    /**
     * @inheritDoc
     */
    public function getConfigTreeBuilder(): TreeBuilder
    {
        $treeBuilder = new TreeBuilder('symfony_livewire');

        $treeBuilder->getRootNode()
            ->children()
                ->scalarNode('uploadDirectory')
                    ->info('The directory relative to the kernel.root_dir to keep temporary uploaded files
from when they are uploaded until your component has processed them. Files older than a day are deleted upon the
next upload request. Path must start with /')
                    ->defaultValue('/files/livewire')
                ->end()
            ->end();

        return $treeBuilder;
    }
}
