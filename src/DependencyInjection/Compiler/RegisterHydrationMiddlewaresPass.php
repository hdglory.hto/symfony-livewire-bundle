<?php
declare(strict_types = 1);

namespace RoflCopter24\SymfonyLivewireBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\ChildDefinition;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\Compiler\PriorityTaggedServiceTrait;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class RegisterHydrationMiddlewaresPass implements CompilerPassInterface
{
    use PriorityTaggedServiceTrait;

    private const TAG_NAME = 'livewire.middleware';

    /**
     * @inheritDoc
     */
    public function process(ContainerBuilder $container): void
    {
        $this->registerMiddlewares($container);
    }

    protected function registerMiddlewares(ContainerBuilder $container): void
    {
        $registry = $container->findDefinition('livewire.middleware.registry');
        $initialHydrationRegistry = $container->findDefinition('livewire.middleware.initial_hydration_registry');
        $initialDehydrationRegistry = $container->findDefinition('livewire.middleware.initial_dehydration_registry');

        foreach ($this->findAndSortTaggedServices(self::TAG_NAME, $container) as $reference) {
            $definition = $container->findDefinition((string) $reference);
            $tags = $definition->getTag(self::TAG_NAME);
            $definition->clearTag(self::TAG_NAME);
            $componentClass = $definition->getClass();

            $initialHydration = $tags[0]['initialHydration'] ?? false;
            $initialDehydration = $tags[0]['initialDehydration'] ?? false;

            $childDefinition = new ChildDefinition((string) $reference);
            $childDefinition->setPublic(true);
            $childDefinition->setAutowired(true);

            $registry->addMethodCall('add', [$componentClass]);
            if ($initialHydration) {
                $initialHydrationRegistry->addMethodCall('add', [$componentClass]);
            }
            if ($initialDehydration) {
                $initialDehydrationRegistry->addMethodCall('add', [$componentClass]);
            }

            $childDefinition->setTags($definition->getTags());
            $container->setDefinition($componentClass, $childDefinition);
        }
    }
}
