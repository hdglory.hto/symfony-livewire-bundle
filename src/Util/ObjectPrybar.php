<?php
declare(strict_types = 1);

namespace RoflCopter24\SymfonyLivewireBundle\Util;

use ReflectionClass;

class ObjectPrybar
{
    /**
     * @var mixed
     */
    protected $obj;

    protected ReflectionClass $reflected;

    public function __construct($obj)
    {
        $this->obj = $obj;
        $this->reflected = new ReflectionClass($obj);
    }

    public function getProperty($name)
    {
        $property = $this->reflected->getProperty($name);

        $property->setAccessible(true);

        return $property->getValue($this->obj);
    }

    public function setProperty($name, $value): void
    {
        $property = $this->reflected->getProperty($name);

        $property->setAccessible(true);

        $property->setValue($this->obj, $value);
    }
}

