<?php
declare(strict_types = 1);

namespace RoflCopter24\SymfonyLivewireBundle\Util;

class NormalizationUtil
{
    /**
     * @param mixed $value The array to reindex. IF not an array, the value will be returned directly.
     * @return mixed The re-indexed array.
     */
    public static function reindexArrayWithNumericKeysOtherwiseJavaScriptWillMessWithTheOrder($value)
    {
        if (!is_array($value)) {
            return $value;
        }

        // Make sure string keys are last (but not ordered) and numeric keys are ordered.
        // JSON.parse will do this on the frontend, so we'll get ahead of it.

        $itemsWithNumericKeys = array_filter($value, static function ($key) {
            return is_numeric($key);
        }, ARRAY_FILTER_USE_KEY);
        ksort($itemsWithNumericKeys);

        $itemsWithStringKeys = array_filter($value, static function ($key) {
            return !is_numeric($key);
        }, ARRAY_FILTER_USE_KEY);

        //array_merge will reindex in some cases so we stick to array_replace
        $normalizedData = array_replace($itemsWithNumericKeys, $itemsWithStringKeys);

        return array_map(static function ($value) {
            return static::reindexArrayWithNumericKeysOtherwiseJavaScriptWillMessWithTheOrder($value);
        }, $normalizedData);
    }
}
