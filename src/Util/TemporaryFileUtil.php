<?php
declare(strict_types=1);

namespace RoflCopter24\SymfonyLivewireBundle\Util;

use Illuminate\Support\Str;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class TemporaryFileUtil
{
    public static function generateHashNameWithOriginalNameEmbedded(UploadedFile $file): string
    {
        $hash = Str::random(30);
        $meta = Str::of('-meta' . base64_encode($file->getClientOriginalName()) . '-')->replace('/', '_');
        $extension = '.'.$file->guessExtension();

        return $hash . $meta . $extension;
    }

    public static function extractOriginalNameFromFilePath(string $path): string
    {
        return base64_decode(head(explode('-', last(explode('-meta', (string)Str::of($path)->replace('_', '/'))))));
    }

    /**
     * Checks if a value can be deserialized by this tool.
     * @param mixed $subject The value to check
     * @return bool true when the value can be deserialized.
     */
    public static function canDeserialize($subject): bool
    {
        if (is_string($subject)) {
            return Str::of($subject)->startsWith(['livewire-file:', 'livewire-files:']);
        }

        if (is_array($subject)) {
            return collect($subject)->contains(function ($value) {
                return self::canDeserialize($value);
            });
        }

        return false;
    }

    /**
     * Deserializes the given value into a representative uploaded file.
     * @param mixed $subject The value to deserialize
     * @param string $uploadsDir The base directory for the uploaded livewire files.
     * @return mixed
     */
    public static function deserializeFromLivewireRequest($subject, string $uploadsDir)
    {
        if (is_string($subject)) {
            if (Str::of($subject)->startsWith('livewire-file:')) {
                return self::createUploadedFileFromLivewire($uploadsDir, (string) Str::of($subject)->after('livewire-file:'));
            }

            if (Str::of($subject)->startsWith('livewire-files:'))  {
                $paths = json_decode((string)Str::of($subject)->after('livewire-files:'), true);
                return collect($paths)->map(function ($path) use ($uploadsDir) { return self::createUploadedFileFromLivewire($uploadsDir, $path); })->toArray();
            }
        }

        if (is_array($subject)) {
            foreach ($subject as $key => $value) {
                $subject[$key] =  self::deserializeFromLivewireRequest($value, $uploadsDir);
            }
        }

        return $subject;
    }

    private static function createUploadedFileFromLivewire(string $basePath, string $fileName): UploadedFile
    {
        $originalName = self::extractOriginalNameFromFilePath($fileName);

        return new UploadedFile("$basePath/$fileName", $originalName);
    }

    /**
     * Serializes a single UploadedFile for livewire client side tracking
     * @param UploadedFile $value
     * @return string
     */
    public static function serializeForLivewireResponse(UploadedFile $value): string
    {
        return 'livewire-file:'.$value->getFilename();
    }

    /**
     * Serializes an array of uploaded files for livewire state tracking.
     * @param UploadedFile[] $files The files to serialize
     * @return string The serialized string
     */
    public static function serializeMultipleForLivewireResponse(array $files): string
    {
        /**
         * @phpstan-ignore-next-line
         */
        return 'livewire-files:'.json_encode(collect($files)->map->getFilename());
    }
}
