<?php
declare(strict_types = 1);

namespace RoflCopter24\SymfonyLivewireBundle\Util;

use JsonException;
use RoflCopter24\SymfonyLivewireBundle\Exception\RootTagMissingFromViewException;

class AttributeUtil
{
    /**
     * Embeds the given attributes and their data into the the root tag
     * of the specified HTML string.
     *
     * @param string $dom  The source HTML.
     * @param array  $data The attribute data to embed.
     * @return string the HTML with the attributes embedded in the root tag.
     * @throws RootTagMissingFromViewException When the component template contains more than one root tag.
     * @throws JsonException When escaping of data failed.
     */
    public static function addAttributesToRootTagOfHtml(string $dom, array $data): string
    {
        $attributesFormattedForHtmlElement = collect($data)
            ->mapWithKeys(function ($value, $key) {
                return [ "wire:${key}" => self::escapeStringForHtml($value) ];
            })
            ->map(function ($value, $key) {
                return sprintf('%s="%s"', $key, $value);
            })
            ->implode(' ');

        preg_match('/<([a-zA-Z0-9\-]*)/', $dom, $matches, PREG_OFFSET_CAPTURE);

        if (count($matches) < 1) {
            throw new RootTagMissingFromViewException(null);
        }

        $tagName = $matches[1][0];
        $tagNameLength = strlen($tagName);
        $firstCharacterPositionInTagName = $matches[1][1];

        return substr_replace(
            $dom,
            ' ' . $attributesFormattedForHtmlElement,
            $firstCharacterPositionInTagName + $tagNameLength,
            0
        );
    }

    /**
     * Escapes a string for use in HTML code.
     *
     * @param mixed $subject The string to escape.
     * @return string The escaped string.
     * @throws JsonException When encoding of json in subject string failed.
     */
    protected static function escapeStringForHtml($subject): string
    {
        if (is_string($subject) || is_numeric($subject)) {
            return htmlspecialchars($subject);
        }

        return htmlspecialchars(json_encode($subject, JSON_THROW_ON_ERROR));
    }
}
