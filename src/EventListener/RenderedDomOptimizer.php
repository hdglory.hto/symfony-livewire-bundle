<?php
declare(strict_types = 1);

namespace RoflCopter24\SymfonyLivewireBundle\EventListener;

use RoflCopter24\SymfonyLivewireBundle\Event\MiddlewareDehydrationEvent;
use RoflCopter24\SymfonyLivewireBundle\Event\MiddlewareHydrationEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class RenderedDomOptimizer implements EventSubscriberInterface
{
    protected array $htmlHashesByComponent = [];

    /**
     * @inheritDoc
     */
    public static function getSubscribedEvents(): array
    {
        return [
            'livewire.component.dehydrate.initial' => ['onInitialDehydration', 0],
            'livewire.component.dehydrate.subsequent' => ['onSubsequentDehydration', 0],
            'livewire.component.hydrate.subsequent' => ['onSubsequentHydration', 0]
        ];
    }

    public function onInitialDehydration(MiddlewareDehydrationEvent $dehydrationEvent): void
    {
        $dehydrationEvent->getResponseData()->memo['htmlHash'] =
            hash('crc32b', $dehydrationEvent->getResponseData()->effects['html']);
    }

    public function onSubsequentDehydration(MiddlewareDehydrationEvent $dehydrationEvent): void
    {
        $oldHash = $this->htmlHashesByComponent[$dehydrationEvent->getComponent()->getId()] ?? null;

        $dehydrationEvent->getResponseData()->memo['htmlHash'] =
            $newHash = hash('crc32b', $dehydrationEvent->getResponseData()->effects['html']);

        if ($oldHash === $newHash) {
            $dehydrationEvent->getResponseData()->effects['html'] = null;
        }
    }

    public function onSubsequentHydration(MiddlewareHydrationEvent $hydrationEvent): void
    {
        $this->htmlHashesByComponent[$hydrationEvent->getComponent()->getId()] =
            $hydrationEvent->getRequestData()->memo['htmlHash'];
    }
}
