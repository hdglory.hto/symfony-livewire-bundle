<?php
/**
 * based on https://github.com/livewire/livewire/blob/v2.3.8/src/RenameMe/SupportEvents.php
 */
declare(strict_types=1);

namespace RoflCopter24\SymfonyLivewireBundle\EventListener;

use RoflCopter24\SymfonyLivewireBundle\Event\MiddlewareDehydrationEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class SupportEvents implements EventSubscriberInterface
{

    /**
     * @inheritDoc
     */
    public static function getSubscribedEvents(): array
    {
        return [
            'livewire.component.dehydrate.initial' => ['onInitialDehydration', 0],
            'livewire.component.dehydrate' => ['onDehydration', 0]
        ];
    }

    public function onInitialDehydration(MiddlewareDehydrationEvent $dehydrationEvent): void
    {
        $dehydrationEvent->getResponseData()->effects['listeners'] = $dehydrationEvent->getComponent()->getEventsBeingListenedFor();
    }

    public function onDehydration(MiddlewareDehydrationEvent $dehydrationEvent): void
    {
        $emits = $dehydrationEvent->getComponent()->getEventQueue();
        $dispatches = $dehydrationEvent->getComponent()->getDispatchQueue();
        $response = $dehydrationEvent->getResponseData();

        if ($emits) {
            $response->effects['emits'] = $emits;
        }

        if ($dispatches) {
            $response->effects['dispatches'] = $dispatches;
        }
    }
}
