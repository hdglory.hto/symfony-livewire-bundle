<?php
/**
 * based on https://github.com/livewire/livewire/blob/v2.3.8/src/RenameMe/SupportRedirects.php
 */
declare(strict_types=1);

namespace RoflCopter24\SymfonyLivewireBundle\EventListener;

use RoflCopter24\SymfonyLivewireBundle\Event\MiddlewareDehydrationEvent;
use RoflCopter24\SymfonyLivewireBundle\Event\MiddlewareHydrationEvent;

class SupportRedirects implements \Symfony\Component\EventDispatcher\EventSubscriberInterface
{

    /**
     * @inheritDoc
     */
    public static function getSubscribedEvents()
    {
        return [
            'livewire.component.hydrate' => ['onHydration', 0],
            'livewire.component.dehydrate' => ['onDehydration', 0]
        ];
    }

    public function onHydration(MiddlewareHydrationEvent $hydrationEvent): void
    {
        // ?
    }

    public function onDehydration(MiddlewareDehydrationEvent $dehydrationEvent): void
    {
        $component = $dehydrationEvent->getComponent();

        if (empty($component->redirectTo)) {
            return;
        }

        $response = $dehydrationEvent->getResponseData();
        $response->effects['redirect'] = $component->redirectTo;
        $response->effects['html'] = $response->effects['html'] ?? '<div></div>';
    }
}
