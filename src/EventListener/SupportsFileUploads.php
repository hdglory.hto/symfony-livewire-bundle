<?php
declare(strict_types=1);

namespace RoflCopter24\SymfonyLivewireBundle\EventListener;

use RoflCopter24\SymfonyLivewireBundle\Component\WithFileUploads;
use RoflCopter24\SymfonyLivewireBundle\Event\PropertyDehydrationEvent;
use RoflCopter24\SymfonyLivewireBundle\Event\PropertyHydrationEvent;
use RoflCopter24\SymfonyLivewireBundle\Service\SettingsService;
use RoflCopter24\SymfonyLivewireBundle\Util\TemporaryFileUtil;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class SupportsFileUploads implements EventSubscriberInterface
{
    private string $uploadBaseDir;

    public function __construct(ParameterBagInterface $parameterBag, SettingsService $settingsService)
    {
        $this->uploadBaseDir = $parameterBag->get('kernel.project_dir').$settingsService->getUploadDir();
    }

    /**
     * @inheritDoc
     */
    public static function getSubscribedEvents(): array
    {
        return [
            'livewire.property.hydrate' => 'onPropertyHydrate',
            'livewire.property.dehydrate' => 'onPropertyDehydrate'
        ];
    }

    public function onPropertyHydrate(PropertyHydrationEvent $hydrationEvent): void
    {
        $uses = array_flip(class_uses_recursive($hydrationEvent->getComponent()));

        if (! in_array(WithFileUploads::class, $uses)) return;

        if (TemporaryFileUtil::canDeserialize($hydrationEvent->getValue())) {
            if (is_array($hydrationEvent->getValue())) {
                $hydrationEvent->getComponent()->{$hydrationEvent->getProperty()} = TemporaryFileUtil::deserializeFromLivewireRequest($hydrationEvent->getValue()[0], $this->uploadBaseDir);
                return;
            }
            $hydrationEvent->getComponent()->{$hydrationEvent->getProperty()} = TemporaryFileUtil::deserializeFromLivewireRequest($hydrationEvent->getValue(), $this->uploadBaseDir);
        }
    }

    public function onPropertyDehydrate(PropertyDehydrationEvent $dehydrationEvent): void
    {
        $uses = array_flip(class_uses_recursive($dehydrationEvent->getComponent()));

        if (! in_array(WithFileUploads::class, $uses)) return;

        $newValue = self::dehydratePropertyFromWithFileUploads($dehydrationEvent->getValue());
        if (is_array($dehydrationEvent->getComponent()->{$dehydrationEvent->getProperty()}) && !is_array($newValue)) {
            $newValue = [$newValue];
        }

        if ($newValue !== $dehydrationEvent->getValue()) {
            $dehydrationEvent->getComponent()->{$dehydrationEvent->getProperty()} = $newValue;
        }
    }

    /**
     * Dehydrates the UploadedFile entry/entries.
     * @param mixed $value
     * @return mixed
     */
    private function dehydratePropertyFromWithFileUploads($value)
    {
        if (TemporaryFileUtil::canDeserialize($value)) {
            return TemporaryFileUtil::deserializeFromLivewireRequest($value, $this->uploadBaseDir);
        }

        if ($value instanceof UploadedFile) {
            return  TemporaryFileUtil::serializeForLivewireResponse($value);
        }

        if (is_array($value) && isset(array_values($value)[0]) && array_values($value)[0] instanceof UploadedFile && is_numeric(key($value))) {
            return TemporaryFileUtil::serializeMultipleForLivewireResponse($value);
        }

        if (is_array($value)) {
            foreach ($value as $key => $item) {
                $value[$key] = self::dehydratePropertyFromWithFileUploads($item);
            }
        }

        return $value;
    }
}
