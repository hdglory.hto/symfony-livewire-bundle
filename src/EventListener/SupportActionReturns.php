<?php
declare(strict_types = 1);

namespace RoflCopter24\SymfonyLivewireBundle\EventListener;

use RoflCopter24\SymfonyLivewireBundle\Event\ActionReturnedEvent;
use RoflCopter24\SymfonyLivewireBundle\Event\MiddlewareDehydrationEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class SupportActionReturns implements EventSubscriberInterface
{
    protected array $returnsByIdAndAction = [];

    /**
     * @inheritDoc
     */
    public static function getSubscribedEvents(): array
    {
        return [
            'livewire.action.returned' => ['onActionReturned'],
            'livewire.component.dehydrate.subsequent' => ['onSubsequentDehydration']
        ];
    }

    public function onActionReturned(ActionReturnedEvent $event): void
    {
        $returned = $event->getValue();
        $componentId = $event->getComponent()->getId();
        if (is_array($returned) || is_numeric($returned) || is_bool($returned) || is_string($returned)) {
            if (! isset($this->returnsByIdAndAction[$componentId])) {
                $this->returnsByIdAndAction[$componentId] = [];
            }

            $this->returnsByIdAndAction[$componentId][$event->getActionName()] = $returned;
        }
    }

    public function onSubsequentDehydration(MiddlewareDehydrationEvent $event): void
    {
        $componentId = $event->getComponent()->getId();
        if (! isset($this->returnsByIdAndAction[$componentId])) {
            return;
        }

        $event->getResponseData()->effects['returns'] = $this->returnsByIdAndAction[$componentId];
    }
}
