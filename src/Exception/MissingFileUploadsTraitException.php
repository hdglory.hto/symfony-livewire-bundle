<?php

namespace RoflCopter24\SymfonyLivewireBundle\Exception;

class MissingFileUploadsTraitException extends \Exception
{
    public function __construct($componentName)
    {
        parent::__construct(
            "Cannot handle file upload without [Livewire\WithFileUploads] trait on the [{$componentName}] component class."
        );
    }
}
