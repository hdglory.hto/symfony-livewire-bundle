<?php
declare(strict_types = 1);

namespace RoflCopter24\SymfonyLivewireBundle\Exception;

class PublicPropertyTypeNotAllowedException extends \Exception
{
    public function __construct(string $componentName, string $key)
    {
        parent::__construct(
            "Livewire component's [{$componentName}] public property [{$key}] must be of type: [numeric, string, array, null, or boolean].\n".
            "Only protected or private properties can be set as other types because JavaScript doesn't need to access them."
        );
    }
}
