<?php
declare(strict_types = 1);

namespace RoflCopter24\SymfonyLivewireBundle\Exception;

use Exception;

class ValidationException extends Exception
{
    private array $errors;

    public function __construct($componentName, $errors)
    {
        $this->errors = $errors;
        parent::__construct("Validation failed for Component ${componentName}");
    }

    /**
     * @return array
     */
    public function getErrors(): array
    {
        return $this->errors;
    }
}
