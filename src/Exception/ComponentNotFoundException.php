<?php
declare(strict_types = 1);

namespace RoflCopter24\SymfonyLivewireBundle\Exception;

use Throwable;

class ComponentNotFoundException extends \Exception
{
 public function __construct($componentName = "", Throwable $previous = null)
 {
     parent::__construct("A livewire component with the name $componentName was not found!", 500, $previous);
 }
}
