<?php
declare(strict_types = 1);

namespace RoflCopter24\SymfonyLivewireBundle\Component;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\MessageBag;
use Illuminate\Support\Str;
use RoflCopter24\SymfonyLivewireBundle\Exception\MissingRulesException;
use RuntimeException;

trait ValidatesInputTrait
{
    use HandlesActionsTrait;

    protected MessageBag $errorBag;

    public function getErrorBag(): MessageBag
    {
        return $this->errorBag ?? new MessageBag;
    }

    public function setErrorBag($bag): MessageBag
    {
        return $this->errorBag = $bag instanceof MessageBag
            ? $bag
            : new MessageBag($bag);
    }

    public function addError($name, $message): MessageBag
    {
        return $this->getErrorBag()->add($name, $message);
    }

    public function resetErrorBag($field = null): MessageBag
    {
        $fields = (array) $field;

        if (empty($fields)) {
            return $this->errorBag = new MessageBag;
        }

        $this->setErrorBag(
            $this->errorBagExcept($fields)
        );

        return $this->getErrorBag();
    }

    public function clearValidation($field = null): void
    {
        $this->resetErrorBag($field);
    }

    public function resetValidation($field = null): void
    {
        $this->resetErrorBag($field);
    }

    public function errorBagExcept($field): MessageBag
    {
        $fields = (array) $field;

        return new MessageBag(
            collect($this->getErrorBag())
                ->reject(function ($messages, $messageKey) use ($fields) {
                    return collect($fields)->some(function ($field) use ($messageKey) {
                        return Str::of($messageKey)->is($field);
                    });
                })
                ->toArray()
        );
    }

    public function rulesForModel($name): Collection
    {
        if (empty($this->getRules())) {
            return collect();
        }

        return collect($this->getRules())
            ->filter(function ($value, $key) use ($name) {
                return $this->beforeFirstDot($key) === $name;
            });
    }

    public function hasRuleFor($dotNotatedProperty): bool
    {
        $propertyWithStarsInsteadOfNumbers = $this->ruleWithNumbersReplacedByStars($dotNotatedProperty);

        // If property has numeric indexes in it,
        if ($dotNotatedProperty !== $propertyWithStarsInsteadOfNumbers) {
            return collect($this->getRules())->keys()->contains($propertyWithStarsInsteadOfNumbers);
        }

        return collect($this->getRules())
            ->keys()
            ->map(function ($key) {
                return (string) Str::of($key)->before('.*');
            })->contains($dotNotatedProperty);
    }

    public function ruleWithNumbersReplacedByStars($dotNotatedProperty): string
    {
        // Convert foo.0.bar.1 -> foo.*.bar.*
        return (string) Str::of($dotNotatedProperty)
            // Replace all numeric indexes with an array wildcard: (.0., .10., .007.) => .*.
            // In order to match overlapping numerical indexes (foo.1.2.3.4.name),
            // We need to use a positive look-behind, that's technically all the magic here.
            // For better understanding, see: https://regexr.com/5d1n3
            ->replaceMatches('/(?<=(\.))\d+\./', '*.')
            // Replace all numeric indexes at the end of the name with an array wildcard
            // (Same as the previous regex, but ran only at the end of the string)
            // For better understanding, see: https://regexr.com/5d1n6
            ->replaceMatches('/\.\d+$/', '.*');
    }

    public function missingRuleFor($dotNotatedProperty): bool
    {
        return !$this->hasRuleFor($dotNotatedProperty);
    }

    public function validate($rules = null, $messages = [], $attributes = []): array
    {
        [ $rules, $messages, $attributes ] = $this->providedOrGlobalRulesMessagesAndAttributes($rules, $messages, $attributes);

        $data = $this->prepareForValidation(
            $this->getDataForValidation($rules)
        );

        $validator = Validator::make($data, $rules, $messages, $attributes);

        $this->shortenModelAttributes($data, $rules, $validator);

        $validatedData = $validator->validate();

        $this->resetErrorBag();

        return $validatedData;
    }

    public function validateOnly($field, $rules = null, $messages = [], $attributes = []): array
    {
        [ $rules, $messages, $attributes ] = $this->providedOrGlobalRulesMessagesAndAttributes($rules, $messages, $attributes);

        // If the field is "items.0.foo", validation rules for "items.*.foo", "items.*", etc. are applied.
        $rulesForField = collect($rules)->filter(function ($rule, $fullFieldKey) use ($field) {
            return Str::of($field)->is($fullFieldKey);
        })->toArray();

        $ruleKeysForField = array_keys($rulesForField);

        $data = $this->prepareForValidation(
            $this->getDataForValidation($rules)
        );

        $validator = Validator::make($data, $rulesForField, $messages, $attributes);

        $this->shortenModelAttributes($data, $rulesForField, $validator);

        $result = $validator->validate();

        $this->resetErrorBag($ruleKeysForField);

        return $result;
    }

    protected function getRules(): array
    {
        if (method_exists($this, 'rules')) {
            return $this->rules();
        }

        if (property_exists($this, 'rules')) {
            return $this->rules;
        }

        return [];
    }

    protected function getMessages(): array
    {
        if (method_exists($this, 'messages')) {
            return $this->messages();
        }

        if (property_exists($this, 'messages')) {
            return $this->messages;
        }

        return [];
    }

    protected function getValidationAttributes(): array
    {
        if (method_exists($this, 'validationAttributes')) {
            return $this->validationAttributes();
        }

        if (property_exists($this, 'validationAttributes')) {
            return $this->validationAttributes;
        }

        return [];
    }

    protected function shortenModelAttributes($data, $rules, $validator): void
    {
        // If a model ($foo) is a property, and the validation rule is
        // "foo.bar", then set the attribute to just "bar", so that
        // the validation message is shortened and more readable.
        foreach ($rules as $key => $value) {
            $propertyName = $this->beforeFirstDot($key);

            /*if (($data[$propertyName] instanceof Entity)
                && Str::of($key)->replace('_', ' ')->is($validator->getDisplayableAttribute($key))) {
                $validator->addCustomAttributes([ $key => $validator->getDisplayableAttribute($this->afterFirstDot($key)) ]);
            }*/
        }
    }

    protected function providedOrGlobalRulesMessagesAndAttributes($rules, $messages, $attributes): array
    {
        $rules = is_null($rules) ? $this->getRules() : $rules;

        if (empty($rules)) {
            throw new MissingRulesException($this->getName());
        }

        $messages = empty($messages) ? $this->getMessages() : $messages;
        $attributes = empty($attributes) ? $this->getValidationAttributes() : $attributes;

        return [ $rules, $messages, $attributes ];
    }

    protected function getDataForValidation($rules): array
    {
        $properties = $this->getPublicPropertiesDefinedBySubClass();

        collect($rules)->keys()
            ->each(function ($ruleKey) use ($properties) {
                $propertyName = $this->beforeFirstDot($ruleKey);

                if (!array_key_exists($propertyName, $properties)) {
                    throw new RuntimeException('No property found for validation: [' . $ruleKey . ']');
                }
            });

        return collect($properties)->map(function ($value) {
            if ($value instanceof Collection/* || $value instanceof PersistentCollection*/) {
                return $value->toArray();
            }

            return $value;
        })->all();
    }

    protected function prepareForValidation($attributes)
    {
        return $attributes;
    }
}
