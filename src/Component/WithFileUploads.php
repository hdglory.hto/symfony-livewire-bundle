<?php
/**
 * Based on the original Laravel livewire source:
 * https://github.com/livewire/livewire/blob/v2.3.8/src/WithFileUploads.php
 */
declare(strict_types=1);

namespace RoflCopter24\SymfonyLivewireBundle\Component;

use RoflCopter24\SymfonyLivewireBundle\Exception\ValidationException;
use RoflCopter24\SymfonyLivewireBundle\Service\SettingsService;
use RoflCopter24\SymfonyLivewireBundle\Util\TemporaryFileUtil;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Exception\DirectoryNotFoundException;
use Symfony\Component\Finder\Finder;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

trait WithFileUploads
{
    use ReceivesEvents;

    public function startUpload($name, $fileInfo, $isMultiple): void
    {
        /**
         * @var UrlGeneratorInterface $urlGenerator
         */
        $temporaryUploadUrl = $this->services->urlGenerator()->generate('livewire.api.file_upload');

        $this->emit('upload:generatedSignedUrl', $name, $temporaryUploadUrl)->self();
    }

    public function finishUpload($propertyName, $tmpPath, $isMultiple): void
    {
        $this->cleanupOldUploads();
        $uploadDir = $this->services->getParameter('kernel.project_dir') . $this->services->get(SettingsService::class)->getUploadDir();;

        if ($isMultiple) {
            $file = collect($tmpPath)->map(function ($path) use ($uploadDir) {
                return new UploadedFile($uploadDir.$path, TemporaryFileUtil::extractOriginalNameFromFilePath($path));
            })->toArray();

            $this->emit('upload:finished', $propertyName, collect($file)->map->getFilename()->toArray())->self();
        } else {
            $file = new UploadedFile($uploadDir.$tmpPath, TemporaryFileUtil::extractOriginalNameFromFilePath($tmpPath));
            $this->emit('upload:finished', $propertyName, [$file->getFilename()])->self();

            // If the property is an array, but the upload ISNT set to "multiple"
            // then APPEND the upload to the array, rather than replacing it.
            if (is_array($value = $this->getPropertyValue($propertyName))) {
                $file = array_merge($value, [$file]);
            }
        }

        $this->syncInput($propertyName, $file);
    }

    public function uploadErrored($name, $errorsInJson, $isMultiple): void
    {
        $this->emit('upload:errored', $name)->self();

        if (is_null($errorsInJson)) {
            $genericValidationMessage = trans('validation.uploaded', ['attribute' => $name]);
            if ($genericValidationMessage === 'validation.uploaded') $genericValidationMessage = "The {$name} failed to upload.";
            throw new ValidationException($this->getName(), [$name => $genericValidationMessage]);
        }

        $errorsInJson = $isMultiple
            ? str_ireplace('files', $name, $errorsInJson)
            : str_ireplace('files.0', $name, $errorsInJson);

        $errors = json_decode($errorsInJson, true)['errors'];

        throw new ValidationException($this->getName(), $errors);
    }

    public function removeUpload($name, $tmpFilename): void
    {
        $uploads = $this->getPropertyValue($name);
        $uploadDir = $this->services->getParameter('kernel.project_dir') . $this->services->get(SettingsService::class)->getUploadDir();;
        $fsHandler = new Filesystem();

        if (is_array($uploads) && isset($uploads[0]) && $uploads[0] instanceof UploadedFile) {
            $this->emit('upload:removed', $name, $tmpFilename)->self();

            $this->syncInput($name, array_values(array_filter($uploads, function ($upload) use ($tmpFilename, $fsHandler, $uploadDir) {
                if ($upload->getFilename() === $tmpFilename) {
                    $fsHandler->remove($uploadDir . $tmpFilename);
                    return false;
                }

                return true;
            })));
        } elseif ($uploads instanceof UploadedFile) {
            $fsHandler->remove($uploadDir . $tmpFilename);

            $this->emit('upload:removed', $name, $tmpFilename)->self();

            if ($uploads->getFilename() === $tmpFilename) $this->syncInput($name, null);
        }
    }

    protected function cleanupOldUploads(): void
    {
        $uploadDir = $this->services->getParameter('kernel.project_dir') . $this->services->get(SettingsService::class)->getUploadDir();;
        $finder = new Finder();

        try {
            $finder->files()->in($uploadDir)->date('before yesterday');

            if (!$finder->hasResults()) {
                return;
            }

            $fsHandler = new Filesystem();
            $fsHandler->remove($finder);
        } catch (DirectoryNotFoundException $exception) {
            return;
        }
    }
}
