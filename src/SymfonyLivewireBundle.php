<?php
declare(strict_types = 1);

namespace RoflCopter24\SymfonyLivewireBundle;

use RoflCopter24\SymfonyLivewireBundle\DependencyInjection\Compiler\RegisterHydrationMiddlewaresPass;
use RoflCopter24\SymfonyLivewireBundle\DependencyInjection\Compiler\RegisterLivewireComponentsPass;
use Symfony\Component\DependencyInjection\Compiler\PassConfig;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class SymfonyLivewireBundle extends Bundle
{
    /**
     * @inheritDoc
     */
    public function build(ContainerBuilder $container): void
    {
        parent::build($container);

        $container->addCompilerPass(new RegisterHydrationMiddlewaresPass(), PassConfig::TYPE_BEFORE_OPTIMIZATION, 100);
        $container->addCompilerPass(new RegisterLivewireComponentsPass('livewire.component'), PassConfig::TYPE_BEFORE_OPTIMIZATION, 101);
    }
}
