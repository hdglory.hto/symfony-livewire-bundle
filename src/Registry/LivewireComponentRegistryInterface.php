<?php
declare(strict_types = 1);

namespace RoflCopter24\SymfonyLivewireBundle\Registry;

interface LivewireComponentRegistryInterface
{
    /**
     * Adds or overrides a LivewireComponent with the specified friendly name and service id.
     *
     * @param string $friendlyName The friendly name of the component.
     * @param string $serviceId    The service id for finding the component in the Symfony container
     * @return $this
     */
    public function add(string $friendlyName, string $serviceId): self;

    /**
     * Removes a LivewireComponent friendly name from the Registry
     *
     * @param string $friendlyName The friendly name of the component.
     * @return $this
     */
    public function remove(string $friendlyName): self;

    /**
     * Checks whether the registry contains a LivewireComponent with the specified name.
     *
     * @param string $friendlyName The friendly name of the component.
     * @return bool
     */
    public function has(string $friendlyName): bool;

    /**
     * Returns the component reference to the LivewireComponent specified by the given name.
     *
     * @param string $friendlyName The friendly name of the component.
     * @return string
     */
    public function get(string $friendlyName): string;

    /**
     * Returns all LivewireComponent serviceIds currently in the registry.
     *
     * @return array
     */
    public function all(): array;

    /**
     * Returns all LivewireComponent friendly names in the registry.
     *
     * @return array
     */
    public function keys(): array;
}
