<?php
declare(strict_types = 1);

namespace RoflCopter24\SymfonyLivewireBundle\Registry;

use RoflCopter24\SymfonyLivewireBundle\Component\LivewireComponentInterface;

class LivewireComponentRegistry implements LivewireComponentRegistryInterface
{
    private array $components = [];

    /**
     * @inheritDoc
     */
    public function add(string $friendlyName, string $serviceId): LivewireComponentRegistryInterface
    {
        $this->components[$friendlyName] = $serviceId;

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function remove(string $friendlyName): LivewireComponentRegistryInterface
    {
        unset($this->components[$friendlyName]);

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function has(string $friendlyName): bool
    {
        return isset($this->components[$friendlyName]);
    }

    /**
     * @inheritDoc
     */
    public function get(string $friendlyName): string
    {
        return $this->components[$friendlyName] ?? '';
    }

    /**
     * @inheritDoc
     */
    public function keys(): array
    {
        return array_keys($this->components);
    }

    /**
     * @inheritDoc
     */
    public function all(): array
    {
        return $this->components;
    }
}
