<?php
declare(strict_types = 1);

namespace RoflCopter24\SymfonyLivewireBundle\Twig;

use Illuminate\Support\Str;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Twig\Environment;
use Twig\Node\Node;
use Twig\Node\TextNode;
use Twig\NodeVisitor\NodeVisitorInterface;

/**
 * Class LivewireNodeVisitor
 *
 * This class handles the replacement of inline HTML wire directives
 * like `<wire:counter foo="bar" />`
 *
 * @package RoflCopter24\SymfonyLivewireBundle\Twig
 */
class LivewireNodeVisitor implements NodeVisitorInterface
{
    private const DIRECTIVE = 'wire';

    private ContainerInterface $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @inheritDoc
     */
    public function enterNode(Node $node, Environment $env): Node
    {
        // filter out actual twig stuff, we only care for the TextNodes -
        // these contain the HTML
        if (!$node instanceof TextNode) {
            return $node;
        }

        // the 'data' attribute of the text node contains the template text.
        if ($text = $node->getAttribute('data')) {
            // if the text does not contain the directive start `<wire:`, just return
            if (!str_contains($text, '<'.self::DIRECTIVE.':')) {
                return $node;
            }

            // Replace all occurrences of the directive with the initial render of the specific component
            $replaced = (string)Str::of($text)
                // Replace all occurrences by calling a closure for each match.
                // Regex is -> <wire:(?'name'[a-zA-Z0-9_-]*)\s(?'args'(?:\w*="\w*"\s)*)\s?\/>
                // (https://regex101.com/r/PKLDOF/4/ if you are curious)
                ->replaceMatches('/<'.self::DIRECTIVE.':(?\'name\'[a-zA-Z0-9_-]*)\s(?\'args\'(?:\w*="\w*"\s?)*)\s?\/>/',
                    /* collect matches using named capture groups, so we get $match['name'] and $match['args'] */
                    function($match) {
                        // if the named capture group 'name' did not match, something is wrong
                        // and just return the entire string
                        if (!$match['name']) {
                            return $match[0];
                        }

                        $componentName = $match['name'];
                        $componentArgs = $match['args']
                            ? Str::of($match['args'])
                                ->explode(' ')
                                ->filter(function (string $input) {
                                    return (bool) $input;
                                })
                                ->map(function (string $argPair) {
                                    return explode('=', $argPair)[1];
                                })
                                ->map(function (string $encapsulatedValue) {
                                    return Str::of($encapsulatedValue)->ltrim('"')->rtrim('"');
                                })
                                ->toArray()
                            : [];

                        // initial component render by the LifecycleManager as usual
                        $componentManager = $this->container->get('livewire.lifecycle_manager');

                        if (!$componentManager) {
                            return '<span>Render error: Livewire component LifecycleManager not found.</span>';
                        }

                        return $componentManager->initialRequest($componentName, ...$componentArgs);
                    }
                );

            $node->setAttribute('data', $replaced);
        }

        return $node;
    }

    /**
     * @inheritDoc
     */
    public function leaveNode(Node $node, Environment $env): ?Node
    {
        return $node;
    }

    /**
     * @inheritDoc
     */
    public function getPriority(): int
    {
        return 10;
    }
}
