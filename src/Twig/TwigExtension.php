<?php
declare(strict_types = 1);

namespace RoflCopter24\SymfonyLivewireBundle\Twig;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Twig\Environment;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class TwigExtension extends AbstractExtension
{
    private ContainerInterface $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @return TwigFunction[]
     */
    public function getFunctions(): array
    {
        return [
            new TwigFunction('livewireStyles', [$this, 'renderStyles'], ['needs_environment' => true, 'is_safe' => ['html']]),
            new TwigFunction('livewireScripts', [ $this, 'renderScripts' ], [ 'needs_environment' => true, 'is_safe' => ['html'] ]),
            new TwigFunction('livewire', [ $this, 'includeComponent' ], [ 'is_safe' => ['html']]),
        ];
    }

    public function getNodeVisitors(): array
    {
        return [
            new LivewireNodeVisitor($this->container) // handler for wire directives `<wire:my-component />`
        ];
    }

    public function renderStyles(Environment $twig): string
    {
        $debug = $this->container->getParameter('kernel.debug');
        return $twig->render('@SymfonyLivewire/styles.html.twig', ['debug' => $debug]);
    }

    public function renderScripts(Environment $twig): string
    {
        $debug = $this->container->getParameter('kernel.debug');
        return $twig->render('@SymfonyLivewire/scripts.html.twig', [
            'debug' => $debug,
            'jsonEncodedOptions' => '',
            'appUrl' => '',
        ]);
    }

    /**
     * Includes a component by returning the initial render.
     *
     * @param string $name The name of the component
     * @param mixed ...$parameters
     * @return string The rendered component HTML
     */
    public function includeComponent(string $name, ...$parameters): string
    {
        $componentManager = $this->container->get('livewire.lifecycle_manager');

        if (!$componentManager) {
            return '<span>Render error: Livewire component LifecycleManager not found.</span>';
        }

        return $componentManager->initialRequest($name, ...$parameters);
    }
}
