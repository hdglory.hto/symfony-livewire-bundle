<?php
declare(strict_types = 1);

namespace RoflCopter24\SymfonyLivewireBundle\Event;

use RoflCopter24\SymfonyLivewireBundle\Component\LivewireComponent;
use RoflCopter24\SymfonyLivewireBundle\Entity\LivewireRequestData;

class PropertyHydrationEvent extends MiddlewareHydrationEvent
{
    /**
     * @var string
     */
    protected string $property;
    /**
     * @var mixed
     */
    protected $value;

    public function __construct(LivewireComponent $component, LivewireRequestData $requestData, string $property, $value)
    {
        parent::__construct($component, $requestData);
        $this->property = $property;
        $this->value = $value;
    }

    /**
     * @return string
     */
    public function getProperty(): string
    {
        return $this->property;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

}
