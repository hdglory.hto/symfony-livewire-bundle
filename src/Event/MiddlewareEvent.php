<?php
declare(strict_types = 1);

namespace RoflCopter24\SymfonyLivewireBundle\Event;

use RoflCopter24\SymfonyLivewireBundle\Component\LivewireComponent;

class MiddlewareEvent
{
    /**
     * @var LivewireComponent
     */
    protected LivewireComponent $component;

    public function __construct(LivewireComponent $component)
    {
        $this->component = $component;
    }

    /**
     * @return LivewireComponent
     */
    public function getComponent(): LivewireComponent
    {
        return $this->component;
    }


}
