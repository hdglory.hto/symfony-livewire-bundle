<?php
/**
 * Based on https://github.com/livewire/livewire/blob/v2.3.8/src/Event.php
 */
declare(strict_types=1);

namespace RoflCopter24\SymfonyLivewireBundle\Event;

class LivewireEvent
{
    protected string $name = '';
    protected array $params = [];
    protected bool $up = false;
    protected bool $self = false;
    protected string $component = '';

    public function __construct($name, $params)
    {
        $this->name = $name;
        $this->params = $params;
    }

    public function up(): LivewireEvent
    {
        $this->up = true;

        return $this;
    }

    public function self(): LivewireEvent
    {
        $this->self = true;

        return $this;
    }

    public function component($name): LivewireEvent
    {
        $this->component = $name;

        return $this;
    }

    public function to(): LivewireEvent
    {
        return $this;
    }

    public function serialize(): array
    {
        $output = [
            'event' => $this->name,
            'params' => $this->params,
        ];

        if ($this->up) $output['ancestorsOnly'] = true;
        if ($this->self) $output['selfOnly'] = true;
        if ($this->component) $output['to'] = $this->component;

        return $output;
    }
}
