<?php
declare(strict_types = 1);

namespace RoflCopter24\SymfonyLivewireBundle\Event;

use RoflCopter24\SymfonyLivewireBundle\Component\LivewireComponent;
use RoflCopter24\SymfonyLivewireBundle\Entity\LivewireResponseData;

class PropertyDehydrationEvent extends MiddlewareDehydrationEvent
{
    /**
     * @var string
     */
    protected string $property;
    /**
     * @var mixed
     */
    protected $value;

    public function __construct(LivewireComponent $component, LivewireResponseData $responseData, string $property, $value)
    {
        parent::__construct($component, $responseData);
        $this->property = $property;
        $this->value = $value;
    }

    /**
     * @return string
     */
    public function getProperty(): string
    {
        return $this->property;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

}
