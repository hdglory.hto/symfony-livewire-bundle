<?php
declare(strict_types = 1);

namespace RoflCopter24\SymfonyLivewireBundle\Event;

use RoflCopter24\SymfonyLivewireBundle\Component\LivewireComponent;

class ActionReturnedEvent extends MiddlewareEvent
{
    /**
     * @var string
     */
    protected string $actionName;
    /**
     * @var mixed
     */
    protected $value;

    public function __construct(LivewireComponent $component, string $actionName, $value)
    {
         parent::__construct($component);
         $this->actionName = $actionName;
         $this->value = $value;
    }

    /**
     * @return string
     */
    public function getActionName(): string
    {
        return $this->actionName;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }
}
