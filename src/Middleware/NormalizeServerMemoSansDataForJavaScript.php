<?php
declare(strict_types = 1);

namespace RoflCopter24\SymfonyLivewireBundle\Middleware;

use RoflCopter24\SymfonyLivewireBundle\Component\LivewireComponent;
use RoflCopter24\SymfonyLivewireBundle\Entity\LivewireRequestData;
use RoflCopter24\SymfonyLivewireBundle\Entity\LivewireResponseData;
use RoflCopter24\SymfonyLivewireBundle\Util\NormalizationUtil;

/**
 * Class NormalizeServerMemoSansDataForJavaScript
 *
 * @package RoflCopter24\SymfonyLivewireBundle\Middleware
 */
class NormalizeServerMemoSansDataForJavaScript implements HydrationMiddlewareInterface
{

    public function hydrate(LivewireComponent $instance, LivewireRequestData $request): void
    {
        //
    }

    public function dehydrate(LivewireComponent $instance, LivewireResponseData $response): void
    {
        foreach ($response->memo as $key => $value)
        {
            if ($key === 'data') {
                continue;
            }

            if (is_array($value)) {
                $response->memo[$key] = NormalizationUtil::reindexArrayWithNumericKeysOtherwiseJavaScriptWillMessWithTheOrder($value);
            }
        }
    }
}
