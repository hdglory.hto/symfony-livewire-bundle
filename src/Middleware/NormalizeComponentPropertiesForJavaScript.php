<?php
declare(strict_types = 1);

namespace RoflCopter24\SymfonyLivewireBundle\Middleware;

use RoflCopter24\SymfonyLivewireBundle\Component\LivewireComponent;
use RoflCopter24\SymfonyLivewireBundle\Entity\LivewireRequestData;
use RoflCopter24\SymfonyLivewireBundle\Entity\LivewireResponseData;
use RoflCopter24\SymfonyLivewireBundle\Util\NormalizationUtil;

/**
 * Class NormalizeComponentPropertiesForJavaScript
 *
 * @package RoflCopter24\SymfonyLivewireBundle\Middleware
 */
class NormalizeComponentPropertiesForJavaScript implements HydrationMiddlewareInterface
{

    public function hydrate(LivewireComponent $instance, LivewireRequestData $request): void
    {
        //
    }

    public function dehydrate(LivewireComponent $instance, LivewireResponseData $response): void
    {
        foreach ($instance->getPublicPropertiesDefinedBySubClass() as $key => $value) {
            if (is_array($value)) {
                $instance->$key = NormalizationUtil::reindexArrayWithNumericKeysOtherwiseJavaScriptWillMessWithTheOrder($value);
            }
            /*if ($value instanceof EloquentCollection) {
                // Preserve collection items order by reindexing underlying array.
                $instance->$key = $value->values();
            }*/
        }
    }
}
