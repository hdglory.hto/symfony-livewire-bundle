<?php
declare(strict_types = 1);

namespace RoflCopter24\SymfonyLivewireBundle\Middleware;

use RoflCopter24\SymfonyLivewireBundle\Component\LivewireComponent;
use RoflCopter24\SymfonyLivewireBundle\Entity\LivewireRequestData;
use RoflCopter24\SymfonyLivewireBundle\Entity\LivewireResponseData;
use RoflCopter24\SymfonyLivewireBundle\Exception\ValidationException;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Class PerformDataBindingUpdates
 *
 * @package RoflCopter24\SymfonyLivewireBundle\Middleware
 */
class PerformDataBindingUpdates implements HydrationMiddlewareInterface
{
    /**
     * @var EventDispatcherInterface
     */
    private EventDispatcherInterface $eventDispatcher;

    public function __construct(EventDispatcherInterface $eventDispatcher)
    {
        $this->eventDispatcher = $eventDispatcher;
    }

    public function hydrate(LivewireComponent $instance, LivewireRequestData $request): void
    {
        try {
            foreach ($request->updates as $update) {
                if ($update['type'] !== 'syncInput') {
                    continue;
                }

                $data = $update['payload'];

                $instance->syncInput($data['name'], $data['value']);
            }
        } catch (ValidationException $e) {
            // TODO: EventDispatcher
            // Livewire::dispatch('failed-validation', $e->validator);
            // $this->eventDispatcher->dispatch($instance, 'livewire.failed_validation');

            $instance->setErrorBag($e->getErrors());
        }
    }

    public function dehydrate(LivewireComponent $instance, LivewireResponseData $response): void
    {
        //
    }
}
