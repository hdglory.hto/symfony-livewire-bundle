<?php
declare(strict_types = 1);

namespace RoflCopter24\SymfonyLivewireBundle\Middleware;

use Illuminate\Support\Arr;
use RoflCopter24\SymfonyLivewireBundle\Component\LivewireComponent;
use RoflCopter24\SymfonyLivewireBundle\Entity\LivewireRequestData;
use RoflCopter24\SymfonyLivewireBundle\Entity\LivewireResponseData;

/**
 * Class HashDataPropertiesForDirtyDetection
 *
 * @package RoflCopter24\SymfonyLivewireBundle\Middleware
 */
class HashDataPropertiesForDirtyDetection implements HydrationMiddlewareInterface
{
    protected array $propertyHashesByComponentId = [];

    public function hydrate(LivewireComponent $instance, LivewireRequestData $request): void
    {
        $data = data_get($request, 'memo.data', []);

        collect($data)->each(function ($value, $key) use ($instance) {
            if (is_array($value)) {
                /** @noinspection SuspiciousLoopInspection */
                foreach (Arr::dot($value, $key.'.') as $dottedKey => $value) {
                    $this->rehashProperty($dottedKey, $value, $instance);
                }
            } else {
                $this->rehashProperty($key, $value, $instance);
            }
        });
    }

    public function dehydrate(LivewireComponent $instance, LivewireResponseData $response): void
    {
        $data = data_get($response, 'memo.data', []);

        $dirtyProps = collect($this->propertyHashesByComponentId[$instance->getId()] ?? [])
            ->filter(function ($hash, $key) use ($data) {
                // Only return the propertyHashes/props that have changed.
                return $this->hash(data_get($data, $key)) !== $hash;
            })
            ->keys()
            ->toArray();

        data_set($response, 'effects.dirty', $dirtyProps);
    }

    public function rehashProperty($name, $value, $component): void
    {
        $this->propertyHashesByComponentId[$component->id][$name] = $this->hash($value);
    }

    public function hash($value)
    {
        if (! is_null($value) && ! is_string($value) && ! is_numeric($value) && ! is_bool($value)) {
            if (is_array($value)) {
                return json_encode($value, JSON_THROW_ON_ERROR);
            }
            $value = method_exists($value, '__toString')
                ? (string) $value
                : json_encode($value, JSON_THROW_ON_ERROR);
        }

        // Using crc32 because it's fast, and this doesn't have to be secure.
        return crc32((string)$value);
    }
}
