<?php
declare(strict_types = 1);

namespace RoflCopter24\SymfonyLivewireBundle\Middleware;

use RoflCopter24\SymfonyLivewireBundle\Component\LivewireComponent;
use RoflCopter24\SymfonyLivewireBundle\Entity\LivewireRequestData;
use RoflCopter24\SymfonyLivewireBundle\Entity\LivewireResponseData;
use RoflCopter24\SymfonyLivewireBundle\Exception\CorruptComponentPayloadException;
use RoflCopter24\SymfonyLivewireBundle\Service\ChecksumService;

/**
 * Class SecureHydrationWithChecksum
 *
 * @package RoflCopter24\SymfonyLivewireBundle\Middleware
 */
class SecureHydrationWithChecksum implements HydrationMiddlewareInterface
{

    /**
     * @var ChecksumService
     */
    private ChecksumService $checksumService;

    public function __construct(ChecksumService $checksumService)
    {
        $this->checksumService = $checksumService;
    }

    public function hydrate(LivewireComponent $instance, LivewireRequestData $request): void
    {
        $checksum = $request->memo['checksum'];

        unset($request->memo['checksum']);

        if (!$this->checksumService->check($checksum, $request->fingerprint, $request->memo)) {
            throw new CorruptComponentPayloadException($instance->getName());
        }
    }

    public function dehydrate(LivewireComponent $instance, LivewireResponseData $response): void
    {
        $response->memo['checksum'] = $this->checksumService->generate($response->fingerprint, $response->memo);
    }
}
