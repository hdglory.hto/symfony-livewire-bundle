<?php
declare(strict_types = 1);

namespace RoflCopter24\SymfonyLivewireBundle\Annotation;

use Terminal42\ServiceAnnotationBundle\Annotation\ServiceTagInterface;

/**
 * @Annotation
 * @Target("CLASS")
 * @Attributes({
 *   @Attribute("initialHydration", type="bool")
 *   @Attribute("initialDehydration", type="bool")
 *   @Attribute("priority", type="int")
 * })
 */
class HydrationMiddleware implements ServiceTagInterface
{
    private array $attributes;

    public function __construct(array $attributes)
    {
        $this->attributes = $attributes;
    }

    public function getName(): string
    {
        return 'livewire.middleware';
    }

    public function getAttributes(): array
    {
        return $this->attributes;
    }
}
