<?php
declare(strict_types = 1);

namespace RoflCopter24\SymfonyLivewireBundle\Entity;

use RoflCopter24\SymfonyLivewireBundle\Util\AttributeUtil;

class LivewireResponseData
{
    public LivewireRequestData $request;

    public array $fingerprint;
    public array $effects;
    public array $memo;

    public function __construct(LivewireRequestData $request)
    {
        $this->request = $request;

        $this->fingerprint = $request->fingerprint;
        $this->memo = $request->memo;
        $this->effects = [];
    }

    /**
     * Creates a new LivewireResponseData instance from request data.
     *
     * @param LivewireRequestData $request the request data to create a response data object
     * @return self the LivewireResponseData instance
     */
    public static function fromRequest(LivewireRequestData $request): self
    {
        return new self($request);
    }

    /**
     * Getter for the component id.
     *
     * @return string the component id
     */
    public function id(): string
    {
        return $this->fingerprint['id'];
    }

    /**
     * embeds the component into the HTML payload.
     */
    public function embedThyselfInHtml(): void
    {
        if (!$html = $this->effects['html'] ?? null) {
            return;
        }

        $this->effects['html'] = AttributeUtil::addAttributesToRootTagOfHtml($html, [
            'initial-data' => $this->toArrayWithoutHtml(),
        ]);
    }

    /**
     * Returns the html of the response, if any.
     *
     * @return string|null html code of the response
     */
    public function html(): ?string
    {
        return $this->effects['html'] ?? null;
    }

    public function setHtml(string $html): void
    {
        $this->effects['html'] = $html;
    }

    /**
     * Embeds the id of the component in the HTML tag
     * (<div wire:id="xxxxx">)
     */
    public function embedIdInHtml(): void
    {
        if (!$html = $this->effects['html'] ?? null) {
            return;
        }

        $this->effects['html'] = AttributeUtil::addAttributesToRootTagOfHtml($html, [
            'id' => $this->fingerprint['id'],
        ]);
    }

    public function toArrayWithoutHtml(): array
    {
        return [
            'fingerprint' => $this->fingerprint,
            'effects' => array_diff_key($this->effects, [ 'html' => null ]),
            'serverMemo' => $this->memo,
        ];
    }

    public function toInitialResponse()
    {
        $this->embedIdInHtml();
        return $this->effects['html'];
    }

    /**
     * Computes the state diff and necessary information for a subsequent response,
     * then returns it.
     *
     * @return array containing the subsequent response information.
     */
    public function toSubsequentResponse(): array
    {
        $this->embedIdInHtml();

        $requestMemo = $this->request->memo;
        $responseMemo = $this->memo;
        $dirtyMemo = [];

        // Only send along the memos that have changed to not bloat the payload.
        foreach ($responseMemo as $key => $newValue) {
            // If the memo key is not in the request, add it.
            if (!isset($requestMemo[$key])) {
                $dirtyMemo[$key] = $newValue;

                continue;
            }

            // If the memo values are the same, skip adding them.
            if ($requestMemo[$key] === $newValue) {
                continue;
            }

            $dirtyMemo[$key] = $newValue;
        }

        // If 'data' is present in the response memo, diff it one level deep.
        if (isset($dirtyMemo['data'], $requestMemo['data'])) {
            foreach ($dirtyMemo['data'] as $key => $value) {
                if ($value === $requestMemo['data'][$key]) {
                    unset($dirtyMemo['data'][$key]);
                }
            }
        }

        // Make sure any data marked as "dirty" is present in the resulting data payload.
        foreach (data_get($this, 'effects.dirty', []) as $property) {
            $property = head(explode('.', $property));

            data_set($dirtyMemo, 'data.' . $property, $responseMemo['data'][$property]);
        }

        return [
            'effects' => $this->effects,
            'serverMemo' => $dirtyMemo,
        ];
    }
}
